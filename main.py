from archive import Archive

if __name__ == '__main__':
    archive = Archive("inventory_lsports-dev_full_14_03_2023_sample1M.parquet")
    print(archive.get_all_games())
    print(archive.get_representative_data("Tennis", 100, 200))