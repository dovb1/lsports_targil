class Singleton(type):
    _instances = {}
    _paths = {}
    def __call__(cls, *args, **kwargs):
        if args[0] not in cls._paths:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
            cls._paths[args[0]] = args[0]
        return cls._instances[cls]