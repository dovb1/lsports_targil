import pandas as pd
from metaclasses.singleton import Singleton
import random


class Archive(metaclass=Singleton):
    def __init__(self, file_path: str):
        self.__df = pd.read_parquet(file_path)
        self.__all_games = {}

    '''
    I assume that function does not need to iterate every time on the the same file path to calculate list of games,
    unless the file path was changed. So I save all games in private property of the class
    '''
    def get_all_games(self):
        if bool(self.__all_games):
            return self.__all_games
        else:
            for key in self.__df.key:
                splited_key = key.split("/")
                sport_type = splited_key[0]
                if sport_type in self.__all_games.keys():
                    self.__all_games[sport_type] += 1
                else:
                    self.__all_games[sport_type] = 1
            return self.__all_games

    def get_representative_data(self, sport: str, amount_of_frames: int, amount_of_games: int):
        sport_random_frames = []
        sport_game_frames = self.__get_sport_frames(sport)

        random_indexes = random.sample(range(0, len(sport_game_frames)), amount_of_games)
        random_game_indexes = random.sample(random_indexes, amount_of_frames)

        for game_index in random_game_indexes:
            sport_random_frames.append(sport_game_frames[game_index])

        return sport_random_frames

    def __get_sport_frames(self, sport: str):
        sport_game_frames = []
        for key in self.__df.key:
            splited_key = key.split("/")
            sport_type = splited_key[0]
            if sport_type == sport:
                sport_game_frames.append(key)
        return sport_game_frames


