import os
import shutil


class FileSystemCacheManager:
    def __init__(self, cache_dir):
        self.cache_dir = cache_dir

    def get(self, key):
        cache_file = os.path.join(self.cache_dir, key)
        if os.path.exists(cache_file):
            with open(cache_file, 'rb') as f:
                return f.read()
        else:
            return None

    def set(self, key, value):
        cache_file = os.path.join(self.cache_dir, key)
        with open(cache_file, 'wb') as f:
            f.write(value)

    def delete(self, key):
        cache_file = os.path.join(self.cache_dir, key)
        if os.path.exists(cache_file):
            os.remove(cache_file)

    def clear(self):
        if os.path.exists(self.cache_dir):
            shutil.rmtree(self.cache_dir)
        os.makedirs(self.cache_dir)
